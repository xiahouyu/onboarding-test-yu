package io.testifi.OnboardingTest;

import io.testifi.cast.testng.DefaultTestRunner;

public class TestAppRegression {

    public static void main(String[] args) throws Exception {
        System.setProperty("xray:testEnvironment", "localRun");
        System.setProperty("xray:executionName", "localTestNG");
        System.setProperty("jira:url", ""); //URL of Jira
        System.setProperty("jira:user", ""); //username for server 4
        System.setProperty("jira:password", ""); //password for server 4
        System.setProperty("jira:xrayAppVersion", ""); //version of Jira either cloud or 4
        System.setProperty("jira:apiToken", ""); //your cloud api token
        System.setProperty("xray:clientId", ""); //your cloud client id
        System.setProperty("xray:clientSecret", ""); //your client secret
        System.setProperty("selectedDevice", "remoteChrome");
        System.setProperty("aws.accessKeyId", ""); //generate temporally token on https://testifi-aws.awsapps.com/
        System.setProperty("aws.secretAccessKey", "");
        DefaultTestRunner.runTestNg("OnboardingTestYu:regression");
    }

}
